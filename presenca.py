import time
import requests
import json
from datetime import datetime

data = {
    "email" : "",
    "password" : ""
}

# requests.get("https://semcomp.icmc.usp.br/api/events").text
events = json.load(open("events.json", "r"))

for event in events:
    now = datetime.now()
    startTime = datetime.strptime(
        event["startDate"][:-5], "%Y-%m-%dT%H:%M:%S")
    diff = startTime-now

    sleep_time = diff.total_seconds()
    if sleep_time > 0:
        print(f"{sleep_time=}")
        time.sleep(int(sleep_time))

    r = requests.post("https://semcomp.icmc.usp.br/api/auth/login", json=data)

    headers = {
        "Authorization": r.headers["Authorization"]
    }

    time.sleep(60)

    s = requests.post(
        f"https://semcomp.icmc.usp.br/api/events/mark-presence/{event['_id']}",
        headers=headers
    )
    
    print(s.status_code, s.text)
